MODD = modd
DEVD = devd

.PHONY: default
default:
	@$(MODD)

.PHONY: start-server
start-server:
	@$(MAKE) -C ./server start

.PHONY: start-client
start-client:
	@$(MAKE) -C ./client start

.PHONY: start-proxy
start-proxy:
	@$(DEVD) -A '10.0.0.2' -p 8080 /=http://localhost:1234 /api/=http://localhost:5000