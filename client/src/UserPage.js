import React from 'react';
import { UserDetails } from "./UserDetails";

export function UserPage() {
    return (
        <div className="pure-g">
            <div className="pure-u-1-1">
                <UserDetails />
            </div>
        </div>
    );
}