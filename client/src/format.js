/**
 * @param {string} value
 * @returns {string}
 */
export function formatDate(value) {
    return new Intl.DateTimeFormat('en-US', {}).format(new Date(value));
}