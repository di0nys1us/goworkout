import React from 'react';
import { LoginForm } from './LoginForm';

/**
 * @param {object} props 
 */
export function LoginPage({ location }) {
    const { from } = location.state || { from: { pathname: '/' } };

    return (
        <div className="pure-g">
            <div className="pure-u-1-1 gw-center">
                <LoginForm from={from} />
            </div>
        </div>
    );
}