export class UserService {

    /**
     * @param {string} email
     * @param {string} password
     * @returns {Promise<User | null>}
     */
    static async authenticate(email, password) {
        if (email && password) {
            const response = await fetch('/api/authenticate', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: new URLSearchParams({
                    email,
                    password,
                }),
            });

            if (response.status === 200) {
                return await response.json();
            }
        }

        return null;
    }

    /**
     * @returns {Promise<User | null>}
     */
    static async reauthenticate() {
        const response = await fetch('/api/reauthenticate', {
            method: 'POST'
        });

        if (response.status === 200) {
            return await response.json();
        }

        return null;
    }

    /**
     * @param {string} userId
     * @returns {Promise<User | null>}
     */
    static async getUserById(userId) {
        const response = await fetch(`/api/users/${userId}`);

        if (response.status === 200) {
            return await response.json();
        }

        return null;
    }
}
