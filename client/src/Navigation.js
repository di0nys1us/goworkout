import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.less';

/**
 * @param {object} props
 * @param {string} props.to
 * @param {string} props.text
 */
function MenuLink({ to, text }) {
    return (
        <li className="pure-menu-item">
            <NavLink className="pure-menu-link" to={to} exact={true}>
                {text}
            </NavLink>
        </li>
    );
}

export function Navigation() {
    return (
        <nav className="pure-menu gw-navigation">
            <NavLink className="pure-menu-heading pure-menu-link" to="/" exact={true}>
                goworkout
            </NavLink>
            <ul className="pure-menu-list">
                <MenuLink to="/exercises" text="Exercises" />
                <MenuLink to="/workouts" text="Workouts" />
            </ul>
        </nav>
    );
}