type ID = string;
type Email = string;
type LocalDate = string;

interface User {
    id: ID;
    email: Email;
    firstName: string;
    lastName: string;
    dateOfBirth: LocalDate;
    roles: string[];
}

interface Session {
    user: User | null;
    setUser?: React.Dispatch<React.SetStateAction<User | null>>;
    isAuthenticated: boolean;
}