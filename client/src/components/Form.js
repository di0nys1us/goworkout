import classNames from 'classnames';
import React from 'react';

/**
 * @param {object} props
 * @param {React.ReactNode} [props.children]
 * @param {React.FormEventHandler<HTMLFormElement>} props.onSubmit
 * @param {boolean} [props.stacked]
 * @param {boolean} [props.aligned]
 */
export function Form(props) {
    const {
        stacked,
        aligned,
        ...rest
    } = props;

    const className = classNames(
        'pure-form',
        {
            'pure-form-stacked': stacked,
            'pure-form-aligned': aligned,
        }
    );

    return (
        <form {...rest} className={className} />
    );
}