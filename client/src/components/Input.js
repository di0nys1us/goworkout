import classNames from 'classnames';
import React from 'react';

/**
 * @param {object} props
 * @param {string} props.name
 * @param {string} props.label
 * @param {boolean} [props.hideLabel]
 * @param {string} [props.type]
 * @param {string} [props.placeholder]
 * @param {boolean} [props.required]
 * @param {React.ChangeEventHandler<HTMLInputElement>} [props.onChange]
 */
export function Input({ name, label, hideLabel, ...rest }) {
    return (
        <div className="pure-control-group">
            <label htmlFor={name} className={classNames({ 'sr-only': hideLabel })}>{label}</label>
            <input
                id={name}
                name={name}
                {...rest}
            />
        </div>
    )
}