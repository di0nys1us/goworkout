import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSession } from '../security';

/**
 * @param {object} props
 */
export function ProtectedRoute({ component: Component, ...rest }) {
    const session = useSession();

    return (
        <Route
            {...rest}
            render={(props) => {
                if (session.isAuthenticated) {
                    return <Component {...props} />;
                } else {
                    return (
                        <Redirect
                            to={{
                                pathname: '/login',
                                state: { from: props.location },
                            }}
                        />
                    );
                }
            }}
        />
    );
}