import classNames from 'classnames';
import React from 'react';

/**
 * @param {object} props
 * @param {React.ReactNode} [props.children]
 * @param {'submit' | 'reset' | 'button'} [props.type]
 * @param {boolean} [props.primary]
 * @param {boolean} [props.active]
 * @param {boolean} [props.disabled]
 */
export function Button(props) {
    const {
        primary,
        active,
        disabled,
        ...rest
    } = props;

    const className = classNames(
        'pure-button',
        {
            'pure-button-primary': primary,
            'pure-button-active': active,
            'pure-button-disabled': disabled,
        },
    );

    return (
        <button {...rest} className={className} />
    );
}