import React, { useEffect, useState } from 'react';
import { UserService } from '../services';
import { SessionContext } from './SessionContext';

/**
 * @param {object} props
 * @param {React.ReactNode} [props.children]
 */
export function SessionProvider({ children }) {
    const [user, setUser] = useState(/** @type {User | null} */(null));

    useEffect(
        () => {
            UserService.reauthenticate()
                .then((data) => {
                    setUser(data);
                })
                .catch(console.log);
        },
        []
    );

    /** @type {Session} */
    const value = {
        user,
        setUser,
        isAuthenticated: !!user,
    }

    return (
        <SessionContext.Provider value={value} children={children} />
    );
}
