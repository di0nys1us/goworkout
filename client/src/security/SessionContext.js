import React, { useContext } from 'react';

export const SessionContext = React.createContext(/** @type {Session} */({ isAuthenticated: false }));

/**
 * @returns {Session}
 */
export function useSession() {
    return useContext(SessionContext);
}