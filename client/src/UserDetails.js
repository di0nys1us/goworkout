import React from 'react';
import { formatDate } from './format';
import { i18n } from './i18n';
import { useSession } from './security';

export function UserDetails() {
    const { user } = useSession();

    if (!user) {
        return null;
    }

    return (
        <dl className="gw-description-list">
            <dt>{i18n`ID:`}</dt>
            <dd>{user.id}</dd>
            <dt>{i18n`Email:`}</dt>
            <dd>
                <a href={`mailto:${user.email}`}>{user.email}</a>
            </dd>
            <dt>First name:</dt>
            <dd>{user.firstName}</dd>
            <dt>Last name:</dt>
            <dd>{user.lastName}</dd>
            <dt>Date of birth:</dt>
            <dd>{formatDate(user.dateOfBirth)}</dd>
            <dt>Roles:</dt>
            <dd>{user.roles.join(', ')}</dd>
        </dl>
    );
}