import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ProtectedRoute } from './components/ProtectedRoute';
import { ExercisesPage } from './ExercisesPage';
import { LoginPage } from './LoginPage';
import { Navigation } from './Navigation';
import { SessionProvider } from './security';
import { UserPage } from './UserPage';
import { WorkoutsPage } from './WorkoutsPage';

function Root() {
    return (
        <div>
            <Navigation />
            <header className="gw-header gw-center">
                <h1>GOWORKOUT</h1>
            </header>
            <main className="gw-main">
                <Switch>
                    <ProtectedRoute
                        path="/"
                        exact={true}
                        component={UserPage}
                    />
                    <ProtectedRoute
                        path="/exercises"
                        exact={true}
                        component={ExercisesPage}
                    />
                    <ProtectedRoute
                        path="/workouts"
                        exact={true}
                        component={WorkoutsPage}
                    />
                    <Route
                        path="/login"
                        exact={true}
                        component={LoginPage}
                    />
                </Switch>
            </main>
        </div>
    );
}

ReactDOM.render(
    <SessionProvider>
        <Router>
            <Root />
        </Router>
    </SessionProvider>,
    document.getElementById('root')
);