import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Form, Input } from './components';
import { useSession } from './security';
import { UserService } from './services';

/**
 * @param {object} props
 * @param {object} [props.from]
 */
export function LoginForm({ from }) {
    const [redirectToReferrer, setRedirectToReferrer] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { setUser } = useSession();

    if (redirectToReferrer) {
        return <Redirect to={from.pathname} />
    }

    return (
        <Form
            stacked={true}
            onSubmit={(event) => {
                event.preventDefault();

                UserService.authenticate(email, password)
                    .then((data) => {
                        setEmail('');
                        setPassword('');

                        if (typeof setUser === 'function') {
                            setUser(data)
                        }

                        setRedirectToReferrer(true);
                    })
                    .catch(console.log);
            }}
        >
            <fieldset>
                <legend className="sr-only">Log in:</legend>
                <Input
                    name="email"
                    label="Email:"
                    hideLabel={true}
                    type="email"
                    required={true}
                    placeholder="Email"
                    onChange={(event) => { setEmail(event.currentTarget.value); }}
                />
                <Input
                    name="password"
                    label="Password:"
                    hideLabel={true}
                    type="password"
                    required={true}
                    placeholder="Password"
                    onChange={(event) => { setPassword(event.currentTarget.value); }}
                />
                <div className="pure-controls">
                    <Button type="submit" primary={true}>
                        Log in
                    </Button>
                </div>
            </fieldset>
        </Form>
    );
}