package web

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/di0nys1us/goworkout/server/service"
	"gitlab.com/di0nys1us/goworkout/server/store"
)

const (
	HeaderContentType = "Content-Type"
	MediaTypeJSON     = "application/json"
)

func NewRouter(db *store.DB) (r *chi.Mux) {
	r = chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.SetHeader(HeaderContentType, MediaTypeJSON))

	userService := service.NewUserService(db)

	// private routes
	r.Group(func(g chi.Router) {
		g.Use(AuthorizationMiddleware)

		g.Post("/users", PostUserHandler(userService))
		g.Get("/users", GetAllUsers(userService))
		g.Get("/users/{userID}", GetUserByIDHandler(userService))
	})

	// public routes
	r.Group(func(g chi.Router) {
		g.Post("/authenticate", AuthenticationHandler(userService))
		g.Post("/reauthenticate", ReauthenticationHandler(userService))
	})

	return
}
