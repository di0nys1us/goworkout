package web

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/di0nys1us/goworkout/server"
)

func PostUserHandler(userService server.UserService) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		newUser := &server.User{}

		err := json.NewDecoder(r.Body).Decode(newUser)

		if err != nil {
			log.Println(err)
			return
		}

		err = userService.CreateUser(r.Context(), newUser)

		if err != nil {
			log.Println(err)
			return
		}
	})
}

func GetAllUsers(userService server.UserService) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		users, err := userService.FindAllUsers(r.Context())

		if err != nil {
			log.Println(err)
			return
		}

		err = json.NewEncoder(w).Encode(users)

		if err != nil {
			log.Println(err)
			return
		}
	})
}

func GetUserByIDHandler(userService server.UserService) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID := chi.URLParam(r, "userID")

		user, err := userService.FindUserByID(r.Context(), userID)

		if err != nil {
			log.Println(err)
			return
		}

		err = json.NewEncoder(w).Encode(user)

		if err != nil {
			log.Println(err)
			return
		}
	})
}
