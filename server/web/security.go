package web

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/rs/xid"

	jwt "github.com/dgrijalva/jwt-go"

	"gitlab.com/di0nys1us/goworkout/server"
)

const (
	cookieName = "access_token"
	secretKey  = "secret"
	audience   = "goworkout"
	issuer     = "goworkout"
)

func resolveSecretKey(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}

	return []byte(secretKey), nil
}

func AuthorizationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c, err := r.Cookie(cookieName)

		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		claims := &server.Claims{}
		token, err := jwt.ParseWithClaims(c.Value, claims, resolveSecretKey)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if !(token.Valid && claims.VerifyAudience(audience, true) && claims.VerifyIssuer(issuer, true)) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), server.ClaimsKey, claims)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func AuthenticationHandler(userService server.UserService) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		email := r.FormValue("email")
		password := r.FormValue("password")

		if email == "" || password == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		user, err := userService.FindUserByEmail(r.Context(), email)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if user == nil {
			log.Printf("user %q not found", email)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		err = bcrypt.CompareHashAndPassword(user.Password, []byte(password))

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		now := time.Now()
		expires := now.AddDate(0, 0, 1)

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, &server.Claims{
			StandardClaims: &jwt.StandardClaims{
				Audience:  audience,
				ExpiresAt: expires.Unix(),
				Id:        xid.NewWithTime(now).String(),
				IssuedAt:  now.Unix(),
				Issuer:    issuer,
				NotBefore: now.Unix(),
				Subject:   string(user.Email),
			},
			Roles: user.Roles,
		})

		key, err := resolveSecretKey(token)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		tokenString, err := token.SignedString(key)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     cookieName,
			Value:    tokenString,
			Expires:  expires,
			Secure:   false,
			HttpOnly: true,
			SameSite: http.SameSiteStrictMode,
		})

		json.NewEncoder(w).Encode(user)
	})
}

func ReauthenticationHandler(userService server.UserService) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c, err := r.Cookie(cookieName)

		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		claims := &server.Claims{}
		token, err := jwt.ParseWithClaims(c.Value, claims, resolveSecretKey)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if !(token.Valid && claims.VerifyAudience(audience, true) && claims.VerifyIssuer(issuer, true)) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), server.ClaimsKey, claims)

		user, err := userService.FindUserByEmail(ctx, claims.Subject)

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		json.NewEncoder(w).Encode(user)
	})
}
