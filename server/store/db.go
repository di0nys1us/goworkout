package store

import (
	"fmt"
	"io"
	"log"

	"github.com/neo4j/neo4j-go-driver/neo4j"
	"gitlab.com/di0nys1us/goworkout/server"
)

type DB struct {
	neo4j.Driver
}

func NewDB(user, password, host, port string) (*DB, error) {
	var driver neo4j.Driver
	var err error

	target := fmt.Sprintf("bolt://%s:%s", host, port)

	if driver, err = neo4j.NewDriver(target, neo4j.NoAuth()); err != nil {
		return nil, err
	}

	return &DB{driver}, nil
}

func LogClose(c io.Closer) {
	err := c.Close()

	if err != nil {
		log.Println(err)
	}
}

func MustGetBytes(record neo4j.Record, key string) []byte {
	if v, ok := record.Get(key); ok {
		switch b := v.(type) {
		case []byte:
			return b
		case string:
			return []byte(b)
		}
	}

	log.Printf("could not get []byte for key = %q\n", key)

	return []byte{}
}

func MustGetString(record neo4j.Record, key string) string {
	if v, ok := record.Get(key); ok {
		if s, ok := v.(string); ok {
			return s
		}
	}

	log.Printf("could not get string for key = %q\n", key)

	return ""
}

func MustGetID(record neo4j.Record, key string) server.ID {
	return server.ID(MustGetString(record, key))
}

func MustGetEmail(record neo4j.Record, key string) server.Email {
	return server.Email(MustGetString(record, key))
}

func MustGetDate(record neo4j.Record, key string) server.Date {
	if v, ok := record.Get(key); ok {
		return server.MustParseDate(v)
	}

	log.Printf("could not get server.Date for key = %q\n", key)

	return server.Date{}
}
