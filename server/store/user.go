package store

import (
	"context"
	"fmt"

	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"

	"github.com/neo4j/neo4j-go-driver/neo4j"
	"gitlab.com/di0nys1us/goworkout/server"
)

const cypherMatch = `
	MATCH
		(user:User %s)
	RETURN
		user.id as id,
		user.email as email,
		user.password as password,
		user.firstName as firstName,
		user.lastName as lastName,
		user.dateOfBirth as dateOfBirth,
		user.roles as roles;
`

func recordToUser(record neo4j.Record) (*server.User, error) {
	var (
		roles server.Roles
	)

	if v, ok := record.Get("roles"); ok {
		if s, ok := v.([]interface{}); ok {
			for _, role := range s {
				if r, ok := role.(string); ok {
					roles = append(roles, server.Role(r))
				}
			}
		}
	}

	user := &server.User{
		ID:          MustGetID(record, "id"),
		Email:       MustGetEmail(record, "email"),
		Password:    MustGetBytes(record, "password"),
		FirstName:   MustGetString(record, "firstName"),
		LastName:    MustGetString(record, "lastName"),
		DateOfBirth: MustGetDate(record, "dateOfBirth"),
		Roles:       roles,
	}

	return user, nil
}

func CreateUser(ctx context.Context, drv neo4j.Driver, user *server.User) (err error) {
	session, err := drv.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}

	defer LogClose(session)

	const cypher = `
		CREATE
			(user:User {
				id: $id,
				email: $email,
				password: $password,
				firstName: $firstName,
				lastName: $lastName,
				dateOfBirth: $dateOfBirth,
				roles: $roles
			})
		RETURN
			ID(user) as id;
	`

	id := xid.New().String()
	password, err := bcrypt.GenerateFromPassword(user.Password, 10)

	if err != nil {
		return err
	}

	_, err = session.WriteTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		_, err := neo4j.Single(tx.Run(cypher, map[string]interface{}{
			"id":          id,
			"email":       user.Email,
			"password":    string(password),
			"firstName":   user.FirstName,
			"lastName":    user.LastName,
			"dateOfBirth": user.DateOfBirth.Neo4j(),
			"roles":       user.Roles,
		}))

		if err != nil {
			return nil, err
		}

		return nil, nil
	})

	if err != nil {
		return err
	}

	user.ID = server.ID(id)
	user.Password = password

	return
}

func findAllUsers(tx neo4j.Transaction) (interface{}, error) {
	result, err := tx.Run(fmt.Sprintf(cypherMatch, "{}"), nil)

	if err != nil {
		return nil, err
	}

	users := []*server.User{}

	for result.Next() {
		user, err := recordToUser(result.Record())

		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	err = result.Err()

	if err != nil {
		return nil, err
	}

	return users, nil
}

func FindAllUsers(ctx context.Context, drv neo4j.Driver) (users []*server.User, err error) {
	session, err := drv.Session(neo4j.AccessModeRead)

	if err != nil {
		return nil, err
	}

	defer LogClose(session)

	result, err := session.ReadTransaction(findAllUsers)

	if err != nil {
		return nil, err
	}

	users = result.([]*server.User)

	return
}

func findUserUsingFilter(filter string, parameters map[string]interface{}) neo4j.TransactionWork {
	return func(tx neo4j.Transaction) (interface{}, error) {
		record, err := neo4j.Single(tx.Run(fmt.Sprintf(cypherMatch, filter), parameters))

		if err != nil {
			return nil, err
		}

		return recordToUser(record)
	}
}

func FindUserByID(ctx context.Context, drv neo4j.Driver, userID string) (user *server.User, err error) {
	session, err := drv.Session(neo4j.AccessModeRead)

	if err != nil {
		return nil, err
	}

	defer LogClose(session)

	u, err := session.ReadTransaction(findUserUsingFilter("{ id: $userId }", map[string]interface{}{"userId": userID}))

	user = u.(*server.User)

	return
}

func FindUserByEmail(ctx context.Context, drv neo4j.Driver, email string) (user *server.User, err error) {
	session, err := drv.Session(neo4j.AccessModeRead)

	if err != nil {
		return nil, err
	}

	defer LogClose(session)

	u, err := session.ReadTransaction(findUserUsingFilter("{ email: $email }", map[string]interface{}{"email": email}))

	user = u.(*server.User)

	return
}
