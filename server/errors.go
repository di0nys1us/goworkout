package server

import (
	"net/http"
)

type httpErrorCode int

// 4xx client error
type clientErrorCode httpErrorCode

// 5xx server error
type serverErrorCode httpErrorCode

var (
	NotFound = clientErrorCode(http.StatusNotFound)
)

type StatusCoder interface {
	StatusCode() int
}

func (err httpErrorCode) StatusCode() int {
	return int(err)
}

func (err httpErrorCode) Error() string {
	return http.StatusText(err.StatusCode())
}

func (err clientErrorCode) Error() string {
	return http.StatusText(int(err))
}

type clientError struct{}

type serverError struct{}
