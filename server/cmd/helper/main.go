package main

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"

	"github.com/rs/xid"
)

func main() {
	fmt.Println(xid.New())

	password, _ := bcrypt.GenerateFromPassword([]byte("secret"), 10)

	fmt.Println(string(password))
}
