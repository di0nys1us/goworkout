package main

import (
	"log"
	"net/http"

	"gitlab.com/di0nys1us/goworkout/server/store"
	"gitlab.com/di0nys1us/goworkout/server/web"
)

func main() {
	db, err := store.NewDB("goworkout", "goworkout", "localhost", "7687")

	if err != nil {
		log.Fatalln(err)
	}

	defer func() {
		err := db.Close()

		if err != nil {
			log.Fatalln(err)
		}
	}()

	err = http.ListenAndServe(":5000", web.NewRouter(db))

	if err != nil {
		log.Fatalln(err)
	}
}
