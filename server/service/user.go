package service

import (
	"context"

	"gitlab.com/di0nys1us/goworkout/server"
	"gitlab.com/di0nys1us/goworkout/server/store"
)

type userService struct {
	*store.DB
}

func (s *userService) CreateUser(ctx context.Context, user *server.User) error {
	return store.CreateUser(ctx, s.DB, user)
}

func (s *userService) UpdateUser(ctx context.Context) error {
	panic("not implemented")
}

func (s *userService) DeleteUser(ctx context.Context) error {
	panic("not implemented")
}

func (s *userService) FindAllUsers(ctx context.Context) ([]*server.User, error) {
	return store.FindAllUsers(ctx, s.DB)
}

func (s *userService) FindUserByID(ctx context.Context, userID string) (*server.User, error) {
	return store.FindUserByID(ctx, s.DB, userID)
}

func (s *userService) FindUserByEmail(ctx context.Context, email string) (*server.User, error) {
	return store.FindUserByEmail(ctx, s.DB, email)
}

func NewUserService(db *store.DB) server.UserService {
	return &userService{db}
}
