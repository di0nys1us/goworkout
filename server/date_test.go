package server_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/di0nys1us/goworkout/server"
)

func TestDate(t *testing.T) {
	const year, month, day = 2010, time.January, 20

	d := server.NewDate(year, month, day)

	assert.Equal(t, year, d.Year())
	assert.Equal(t, month, d.Month())
	assert.Equal(t, day, d.Day())

	b, err := json.Marshal(d)

	assert.NoError(t, err)
	assert.Equal(t, `"2010-01-20"`, string(b))

	err = json.Unmarshal([]byte(`"2000-10-30"`), &d)

	assert.NoError(t, err)
	assert.Equal(t, 2000, d.Year())
	assert.Equal(t, time.October, d.Month())
	assert.Equal(t, 30, d.Day())
}
