package server

import (
	"context"

	"github.com/dgrijalva/jwt-go"
)

const (
	ClaimsKey claimsKeyType = "ClaimsKey"
)

type claimsKeyType string

type Claims struct {
	*jwt.StandardClaims
	Roles Roles `json:"rls,omitempty"`
}

func GetClaims(ctx context.Context) *Claims {
	if claims, ok := ctx.Value(ClaimsKey).(*Claims); ok {
		return claims
	}

	return nil
}
