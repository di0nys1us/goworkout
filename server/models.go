package server

import (
	"database/sql/driver"
	"encoding/json"
	"net/url"
	"time"
)

// An ID represents an entity's unique ID.
type ID string

func (id ID) String() string {
	return string(id)
}

// An Email represents a user's email address.
type Email string

func (email Email) String() string {
	return string(email)
}

// A User doing workouts.
type User struct {
	ID          ID     `json:"id"`
	Email       Email  `json:"email"`
	Password    []byte `json:"-"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	DateOfBirth Date   `json:"dateOfBirth"`
	Roles       Roles  `json:"roles"`
}

// A Role is a named user role.
type Role string

func (role Role) String() string {
	return string(role)
}

// Roles is a collection of user roles.
type Roles []Role

func (r *Roles) Scan(src interface{}) (err error) {
	if data, ok := src.([]byte); ok {
		err = json.Unmarshal(data, r)
	}

	return
}

func (r *Roles) Value() (value driver.Value, err error) {
	if r == nil {
		return
	}

	value, err = json.Marshal(r)

	return
}

// A WorkoutSession represents a workout session on a specific date.
type WorkoutSession struct {
	ID             ID      `json:"id"`
	Date           Date    `json:"date"`
	Metrics        Metrics `json:"metrics"`
	UserID         ID      `json:"userId"`
	WorkoutGroupID ID      `json:"workoutGroupId"`
}

// A WorkoutGroup represents a node of a hierarchichal workout plan or session.
// Examples include root level nodes like 'The 6-Week Model Body Workout Plan' or 'Huk 2019' or higher
// level nodes like 'Day One', 'Week Two', 'Mondays', or 'Chest and triceps workout'
// or leaf nodes like 'Exercise 1' or 'Bench Press'.
type WorkoutGroup struct {
	ID         ID     `json:"id"`
	Name       string `json:"name"`
	ParentID   ID     `json:"parentId"`
	ExerciseID ID     `json:"exerciseId"`
}

// WorkoutGroups is a collection of workout groups.
type WorkoutGroups []WorkoutGroup

// An ExerciseSet represents a set in an workout group.
type ExerciseSet struct {
	ID             ID  `json:"id"`
	Number         int `json:"number"`
	Duration       int `json:"duration"`
	Repetitions    int `json:"repetitions"`
	Rest           int `json:"rest"`
	WorkoutGroupID ID  `json:"workoutGroupId"`
}

// ExerciseSets is a collection of exercise sets.
type ExerciseSets []ExerciseSet

// An Exercise represents a distinct workout exercise.
// Examples include 'Deadlift', 'Swimming' or 'Burpee'.
type Exercise struct {
	ID          ID       `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Thumbnail   *url.URL `json:"thumbnail"`
	URL         *url.URL `json:"url"`
}

// Exercises is a collection of exercises.
type Exercises []Exercise

// A Metric represents a measure related to e.g. a workout session or an exercise set.
type Metric struct {
	ID          ID     `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Value       string `json:"value"`
	Unit        string `json:"unit"`
}

// Metrics is a collection of metrics.
type Metrics []Metric

// An Event represents a system or entity related event.
type Event struct {
	ID          ID        `json:"id"`
	Timestamp   time.Time `json:"timestamp"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	UserID      ID        `json:"userId"`
}

// Events is a collection of events.
type Events []Event
