package server

import (
	"encoding/json"
	"log"
	"reflect"
	"time"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

const (
	layout = "2006-01-02"
)

type Date struct {
	time.Time
}

func (date Date) String() string {
	return date.Format(layout)
}

func (date Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(date.String())
}

func (date *Date) UnmarshalJSON(data []byte) (err error) {
	if data == nil {
		return
	}

	var value string

	err = json.Unmarshal(data, &value)

	if err != nil {
		return
	}

	t, err := time.Parse(layout, value)

	if err != nil {
		return
	}

	date.Time = t

	return
}

func (date Date) Neo4j() neo4j.Date {
	return neo4j.DateOf(date.Time)
}

func NewDate(year int, month time.Month, day int) Date {
	return Date{Time: time.Date(year, month, day, 0, 0, 0, 0, time.UTC)}
}

func MustParseDate(date interface{}) Date {
	switch v := date.(type) {
	case Date:
		return v
	case *Date:
		return *v
	case time.Time:
		return Date{Time: v}
	case neo4j.Date:
		return Date{Time: v.Time()}
	case string:
		t, _ := time.Parse(layout, v)
		return Date{Time: t}
	default:
		log.Println(reflect.TypeOf(date))
		return Date{}
	}
}
