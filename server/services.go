package server

import (
	"context"
)

type UserService interface {
	CreateUser(ctx context.Context, user *User) error
	UpdateUser(ctx context.Context) error
	DeleteUser(ctx context.Context) error
	FindAllUsers(ctx context.Context) ([]*User, error)
	FindUserByID(ctx context.Context, userID string) (*User, error)
	FindUserByEmail(ctx context.Context, email string) (*User, error)
}

type WorkoutSessionService interface {
	CreateWorkoutSession(ctx context.Context) error
}
